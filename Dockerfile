FROM tomcat:8.0.41-jre8

ADD ./target/SSN.war /usr/local/tomcat/webapps/
CMD ["catalina.sh", "run"]