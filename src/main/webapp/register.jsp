    <form id="register-form" action="register.action" method="POST" role="form" style="display: none;">
        <div class="form-group">
            <input type="text" class="form-control" name="username" placeholder="Username"/>
        </div>
        <div class="form-group">
            <input type="password" class="form-control" name="password" placeholder="Password *"/>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="firstName" placeholder="First Name *"/>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="lastName" placeholder="Last Name *"/>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="email" placeholder="Email"/>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Register Now">
                </div>
            </div>
        </div>
    </form>