function removePost(postCtr) {
    var datastring = "postId=" + $(postCtr).find(".postId").val();
    $.ajax({
        type: "POST",
        url: "deletepost.action",
        data: datastring,
        dataType: "html",
        success: function(data) {
            $(postCtr).remove();
        },
        error: function() {
            alert('error handling here');
        }
    });
}

function unfollow(friendship) {
    var datastring = "friendId=" + $(friendship).find(".friendId").val();
    $.ajax({
        type: "POST",
        url: "unfollow.action",
        data: datastring,
        dataType: "html",
        success: function(data) {
            $(friendship).remove();
        },
        error: function() {
            alert('error handling here');
        }
    });
}