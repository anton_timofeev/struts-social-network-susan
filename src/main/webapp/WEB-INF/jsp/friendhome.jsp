<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/lib/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <script src="js/lib/jquery-3.3.1.js"></script>
    <script src="js/lib/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/custom.css"/>
    <script src="js/custom.js"></script>
    <title>${friend.username} wall</title>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-2 col-md-offset-1">
            <s:set var="active" value="'friends'"/>
            <s:include value="partitions/sideMenu.jsp"/>
        </div>
        <div class="col-md-8">
            <span class="jumbotron">
            You are at ${friend.username}'s wall
            </span>
            <s:set var="isNotMyWall" value="'true'"/>
            <s:iterator value="friend.walls" var="wall">
                <jsp:include page="partitions/wall.jsp">
                    <jsp:param name="posts" value="${wall.posts}"></jsp:param>
                    <jsp:param name="wallId" value="${wall.id}"></jsp:param>
                    <jsp:param name="userId" value="${friend.id}"></jsp:param>
                </jsp:include>
            </s:iterator>
        </div>
    </div>
</div>


</body>
</html>