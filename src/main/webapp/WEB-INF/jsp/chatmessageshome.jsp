<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/lib/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <script src="js/lib/jquery-3.3.1.js"></script>
    <script src="js/lib/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
    <link rel="stylesheet" type="text/css" href="css/custom.css"/>

    <title>Chat</title>
    <script src="js/lib/jquery.socket.io.js"></script>
    <script>
        var chat;
        var ChatObject = function(socketVal) {
            this.socketInstance = socketVal;
            this.sendMessage = function(messageVal) {
              this.socketInstance.send({
                  "message": messageVal,
                  "userId": ${user.id},
                  "chatId": ${chat.id},
                  "userName": "${user.username}"
              }).done(function() {
                  console.log('message send');
              }).fail(function(e) {
                  // your error handling e. g.
                  console.log(e);
              });
            };
        };
        $(document).ready( function () {
            var requestURL = '${pageContext.request.requestURL}';
            requestURL = requestURL.replace('http://', '');
            requestURL = requestURL.replace('https://', '');
            requestURL = requestURL.substring(0, requestURL.indexOf('/'));
            var ctxPath = '${pageContext.request.contextPath}';
            if (ctxPath !== '/') {
                requestURL += ctxPath;
            }
            var socketVal = $.simpleWebSocket({ url: 'ws://' + requestURL + '/websocket/chat/${chat.id}' });

            socketVal.listen(function(message) {
                var newDiv =
                    '<div class="message panel blue ' +
                    (message.userId == ${user.id} ? 'rightblock' : 'leftblock grey') +
                    '">\n' +
                    '<input type="hidden" name="" value="${message.id}" class="messageId">  \n' +
                    '    <div class="header panel-heading">\n' +
                    '        <div class="row">\n' +
                    '            <span class="col-sm-3 col-sm-offset-8">\n' +
                    message.dateFormatted +
                    '            </span>\n' +/*
                    '            <div class="col-sm-1">\n' +
                    '                <button class="btn btn-sm btn-default" style="padding-bottom: 1px; padding-top: 1px;" onclick="removeMessage($(this).closest(\'.message\'))">x</button>\n' +
                    '            </div>\n' +*/
                    '        </div>\n' +
                    '    </div>\n' +
                    '    <div class="content panel-body">\n' +
                    message.message +
                    '    </div>\n' +
                    '</div>';
                $("#newmessage").after(newDiv);
                $("#newmessageform #message").val("");
            })
            chat = new ChatObject(socketVal);
        });
    </script>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-2 col-md-offset-1">
            <s:set var="active" value="'chats'"/>
            <s:include value="partitions/sideMenu.jsp"/>
        </div>
        <div class="col-md-8">
            <span class="jumbotron">
            Chat with ${user.id eq chat.user1.id ? chat.user2.username : chat.user1.username}
            </span>
            <div id="messagewall">
                <div id="newmessage" class="row">
                    <div class="col-lg-12">
                        <s:include value="partitions/newmessage.jsp"/>
                    </div>
                </div>
                <s:iterator value="messages" var="messageObj">
                    <s:include value="partitions/message.jsp"/>
                </s:iterator>
            </div>
        </div>
    </div>
</div>

</body>
</html>