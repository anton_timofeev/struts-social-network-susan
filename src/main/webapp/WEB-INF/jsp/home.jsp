<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/lib/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <script src="js/lib/jquery-3.3.1.js"></script>
    <script src="js/lib/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
    <link rel="stylesheet" type="text/css" href="css/custom.css"/>

    <title>Wall</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-md-offset-1">
                <s:set var="active" value="'home'"/>
                <s:include value="partitions/sideMenu.jsp"/>
            </div>
            <div class="col-md-8">
                <s:iterator value="user.walls" var="wall">
                    <s:include value="partitions/wall.jsp"/>
                </s:iterator>
            </div>
        </div>
    </div>
</body>
</html>