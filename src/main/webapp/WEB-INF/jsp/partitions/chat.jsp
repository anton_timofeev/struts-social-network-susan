<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="chat panel">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-12">
                <a href="chat.action?friendId=${chat.friend.id}"><s:property value="#chat.friend.username"/></a>
            </div>
        </div>
    </div>
</div>