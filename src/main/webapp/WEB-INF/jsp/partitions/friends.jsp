<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="friends">
    <s:iterator var="friend" value="friends">
        <div class="friend panel">
            <input type="hidden" class="friendId" value="${friend.id}"/>
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-10">
                    <a href="gotofriend.action?friendId=${friend.id}"><s:property value="#friend.username"/></a>
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-sm btn-default" onclick="unfollow($(this).closest('.friend'))">Unfollow</button>
                    </div>
                </div>
            </div>
        </div>
    </s:iterator>
</div>