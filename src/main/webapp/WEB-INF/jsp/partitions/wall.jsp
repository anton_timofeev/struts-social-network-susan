<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="wall">
    <div class="row">
        <div class="col-lg-12">
            <s:include value="newpost.jsp"/>
        </div>
    </div>
    <div class="posts">
        <s:iterator value="%{#wall.posts}" var="post">
            <s:include value="post.jsp"/>
        </s:iterator>
    </div>
</div>