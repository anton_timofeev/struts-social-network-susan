<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="sideMenu list-group">
    <a href="myhome.action" class="list-group-item <s:if test="%{#active == 'home'}">active</s:if>">Home</a>
    <a href="myfriends.action" class="list-group-item <s:if test="%{#active == 'friends'}">active</s:if>">Friends</a>
    <s:if test="%{#active == 'friends' or #search == 'friends'}">
        <a href="findfriend.action"
           style="margin-left: 15px;"
           class="list-group-item <s:if test="%{#search == 'friends'}">active</s:if>">Find...<%--
        --%></a>
    </s:if>
    <a href="allchats.action" class="list-group-item <s:if test="%{#active == 'chats'}">active</s:if>">Messages</a>
    <a href="logout.action" class="list-group-item">Logout</a>
</div>