<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="post panel blue">
    <s:hidden class="postId" value="%{#post.id}"/>
    <div class="header panel-heading">
        <s:set var="createdDate" value="#post.createdDate"/>
        <div class="row textright">
            <span class="col-sm-10">
                <s:property value="#post.createdBy.username"/>, <s:date name="createdDate" format="dd/MM/yyyy HH:mm"/>
            </span>
            <div class="col-sm-2">
                <button class="btn btn-sm btn-default" style="padding-bottom: 1px; padding-top: 1px;" onclick="removePost($(this).closest('.post'))">x</button>
            </div>
        </div>
    </div>
    <hr>
    <div class="content panel-body">
        <div class="row">
            <span class="col-sm-11">
                <s:property value="#post.content"/>
            </span>
        </div>
    </div>
</div>