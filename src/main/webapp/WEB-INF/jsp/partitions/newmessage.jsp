<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="newmessage row">
    <script>
        $(document).ready(function() {
            $("#newmessageform").submit(function(event) {
               event.preventDefault();
                var datastring = $(this).serialize();
                /*$.ajax({
                    type: "POST",
                    url: "newmessage.action",
                    data: datastring,
                    dataType: "html",
                    success: function(data) {
                        if ($(".messages .message").length == 0) {
                            $($(".messages")[0]).html(data);
                        } else {
                            $($(".messages .message")[0]).before(data);
                        }
                        $("#message").val("");
                    },
                    error: function() {
                        alert('error handling here');
                    }
                });*/
                chat.sendMessage($(this).find('#message').val());
            });
        });
    </script>
    <div class="jumbotron col-lg-12">
    <form id="newmessageform" action="newmessage.action" method="POST" style="display: block;">
        <input type="hidden" id="userId" name="userId" value="%{user.id}"/>
        <input type="hidden" id="chatId" name="chatId" value="%{chat.id}"/>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <textarea id="message" name="message" placeholder="Tell something..."  class="form-control"></textarea>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <input type="submit" name="post-submit" id="post-submit" class="btn btn-sm btn-success col-sm-3 col-sm-offset-6" value="Send">
                        <input type="button" value="Clear" class="btn btn-sm col-sm-3" onclick="$('#newmessageform #message').val('');"/>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>