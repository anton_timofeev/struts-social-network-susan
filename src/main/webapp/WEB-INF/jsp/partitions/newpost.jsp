<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="newpost row">
    <script>
        $(document).ready(function() {
            $("#newpostform").submit(function(event) {
               event.preventDefault();
                var datastring = $(this).serialize();
                $.ajax({
                    type: "POST",
                    url: "newpost.action",
                    data: datastring,
                    dataType: "html",
                    success: function(data) {
                        if ($(".posts .post").length == 0) {
                            $($(".posts")[0]).html(data);
                        } else {
                            $($(".posts .post")[0]).before(data);
                        }
                        $("#content").val("");
                    },
                    error: function() {
                        alert('error handling here');
                    }
                });
            });
        });
    </script>
    <div class="jumbotron col-lg-12">
        <form id="newpostform" action="newpost.action" method="POST" style="display: block;">
            <input type="hidden" id="userId" name="userId" value="${user.id}"/>
            <input type="hidden" id="wallId" name="wallId" value="${wall.id}"><s:property value="#parameters.wallId"/></input type="hidden">
            <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                <textarea id="content" name="content" placeholder="Tell something..."  class="form-control"></textarea>
                </div>
            </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                        <input type="submit" name="post-submit" id="post-submit" class="btn btn-sm btn-success col-sm-3 col-sm-offset-6" value="Post">
                        <input type="button" value="Clear" class="btn btn-sm col-sm-3"/>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>