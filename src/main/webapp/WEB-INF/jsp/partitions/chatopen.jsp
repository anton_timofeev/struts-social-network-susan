<%@ taglib prefix="s" uri="/struts-tags" %>

<div class="friend">
    <s:hidden class="friendship" value="%{friendship.id}"/>
    <a href="chat.action?friendship=${friendship.id}"><s:property value="#friend.username"/></a>
    <div>
        <button onclick="unfollow($(this).closest('.friend'))">Delete</button>
    </div>
</div>