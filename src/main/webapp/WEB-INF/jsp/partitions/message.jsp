<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="message panel blue ${user.id eq messageObj.createdBy.id ? 'rightblock' : 'leftblock grey'}">
    <s:hidden class="messageId" value="%{#messageObj.id}"/>
    <div class="header panel-heading">
        <div class="row">
            <span class="col-sm-3 col-sm-offset-8">
                ${messageObj.dateFormatted}
            </span><%--
            <div class="col-sm-1">
                <button class="btn btn-sm btn-default" style="padding-bottom: 1px; padding-top: 1px;" onclick="removeMessage($(this).closest('.message'))">x</button>
            </div>--%>
        </div>
    </div>
    <div class="content panel-body">
        <s:property value="#messageObj.message"/>
    </div>
</div>