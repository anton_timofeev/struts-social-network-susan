<%@ taglib prefix="s" uri="/struts-tags" %>
<script>
    function follow(userId) {

           var datastring = "friendId=" + userId;
           $.ajax({
               type: "POST",
               url: "follow.action",
               data: datastring,
               success: function() {
                   $("#avail" + userId).remove();
               },
               error: function() {
                   alert('error handling here');
               }
           });
    }
</script>
<div class="friends">
    <s:iterator var="friend" value="availableusers">
        <div class="avail panel" id="avail${friend.id}">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-10">
                    <a href="gotofriend.action?friendId=${friend.id}">${friend.username}</a>
                    </div>
                    <div class="col-sm-2">
                        <input type="button" onclick="follow(${friend.id})" class="btn btn-sm btn-default" value="Follow"/>
                    </div>
                </div>
            </div>
        </div>
    </s:iterator>
</div>
