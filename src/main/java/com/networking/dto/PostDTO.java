package com.networking.dto;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "posts")

@NamedNativeQueries({
        @NamedNativeQuery(
                name = "deletePostById",
                query = "delete " +
                        "FROM posts where id = :id"
        )
})
public class PostDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "wall_id", nullable = false)
    private WallDTO wall;

    @Column(name = "created_date")
    private Date createdDate;

    @ManyToOne(cascade = CascadeType.ALL, targetEntity = UserDTO.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by")
    private UserDTO createdBy;

    private String content;

    public Integer getId() {
        return id;
    }

    public PostDTO setId(Integer id) {
        this.id = id;
        return this;
    }

    public WallDTO getWall() {
        return wall;
    }

    public PostDTO setWall(WallDTO wall) {
        this.wall = wall;
        return this;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public PostDTO setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public UserDTO getCreatedBy() {
        return createdBy;
    }

    public PostDTO setCreatedBy(UserDTO createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public String getContent() {
        return content;
    }

    public PostDTO setContent(String content) {
        this.content = content;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PostDTO)) return false;
        PostDTO postDTO = (PostDTO) o;
        return Objects.equals(id, postDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
