package com.networking.dto;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "walls")

@NamedNativeQueries({
        @NamedNativeQuery(
                name = "getWallById",
                query = "SELECT * " +
                        "FROM walls where id = :wallId",
                resultClass=WallDTO.class
        )
})
public class WallDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserDTO user;

    @OneToMany(mappedBy = "wall", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @OrderBy("createdDate desc")
    private List<PostDTO> posts = new ArrayList<>(0);

    public Integer getId() {
        return id;
    }

    public WallDTO setId(Integer id) {
        this.id = id;
        return this;
    }

    public List<PostDTO> getPosts() {
        return posts;
    }

    public WallDTO setPosts(List<PostDTO> posts) {
        this.posts = posts;
        return this;
    }

    public UserDTO getUser() {
        return user;
    }

    public WallDTO setUser(UserDTO user) {
        this.user = user;
        return this;
    }
}
