package com.networking.dto;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "users")

@NamedNativeQueries({
        @NamedNativeQuery(
                name = "getUserByUsername",
                query = "SELECT * " +
                        "FROM users where username = :username",
                resultClass=UserDTO.class
        ),
        @NamedNativeQuery(
                name = "getUserByUsernamePassword",
                query = "SELECT * " +
                        "FROM users where username = :username and hashpassword = :password",
                resultClass=UserDTO.class
        ),
        @NamedNativeQuery(
                name = "getUserById",
                query = "SELECT * " +
                        "FROM users where id = :userId",
                resultClass=UserDTO.class
        ),
        @NamedNativeQuery(
                name = "getNotFriends",
                query = "SELECT * " +
                        "FROM users u where u.id <> :userId and u.id not in " +
                        " ( select friend_id from friends f where f.user_id = :userId ) ",
                resultClass=UserDTO.class
        )
})

public class UserDTO implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "username")
    private String username;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    private String email;

    private byte[] salt;

    @Column(name = "hashpassword")
    private byte[] hashPassword;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<FriendDTO> friends = new HashSet<>(0);

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<WallDTO> walls = new HashSet<>(0);

    public Set<WallDTO> getWalls() {
        return walls;
    }

    public UserDTO setWalls(Set<WallDTO> walls) {
        this.walls = walls;
        return this;
    }

    public Set<FriendDTO> getFriends() {
        return friends;
    }


    public UserDTO setFriends(Set<FriendDTO> friends) {
        this.friends = friends;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public UserDTO setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public UserDTO setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public UserDTO setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public UserDTO setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UserDTO setEmail(String email) {
        this.email = email;
        return this;
    }

    public byte[] getSalt() {
        return salt;
    }

    public UserDTO setSalt(byte[] salt) {
        this.salt = salt;
        return this;
    }

    public byte[] getHashPassword() {
        return hashPassword;
    }

    public UserDTO setHashPassword(byte[] hashPassword) {
        this.hashPassword = hashPassword;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserDTO)) return false;
        UserDTO userDTO = (UserDTO) o;
        return Objects.equals(username, userDTO.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }
}
