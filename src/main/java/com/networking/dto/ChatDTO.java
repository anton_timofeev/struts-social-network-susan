package com.networking.dto;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "chats")

@NamedNativeQueries({
        @NamedNativeQuery(
                name = "findChatByUsers",
                query = "select * from chats where (user_id_1 = :userId1 and user_id_2 = :userId2) " +
                        " or (user_id_1 = :userId2 and user_id_2 = :userId1) ",
                resultClass = ChatDTO.class
        ),
        @NamedNativeQuery(
                name = "findChatById",
                query = "select * from chats where id = :id",
                resultClass = ChatDTO.class
        ),
        @NamedNativeQuery(
                name = "createChat",
                query = "insert into chats (user_id_1, user_id_2) values (:userId1, :userId2)"
        )
})
public class ChatDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "user_id_1")
    private UserDTO user1;

    @ManyToOne
    @JoinColumn(name = "user_id_2", nullable = false)
    private UserDTO user2;

    @OneToMany(mappedBy = "chat", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<MessageDTO> messages;

    public Integer getId() {
        return id;
    }

    public ChatDTO setId(Integer id) {
        this.id = id;
        return this;
    }

    public UserDTO getUser1() {
        return user1;
    }

    public ChatDTO setUser1(UserDTO user1) {
        this.user1 = user1;
        return this;
    }

    public UserDTO getUser2() {
        return user2;
    }

    public ChatDTO setUser2(UserDTO user2) {
        this.user2 = user2;
        return this;
    }

    public List<MessageDTO> getMessages() {
        return messages;
    }

    public ChatDTO setMessages(List<MessageDTO> messages) {
        this.messages = messages;
        return this;
    }
}
