package com.networking.dto;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "messages")

@NamedNativeQueries({
        @NamedNativeQuery(
                name = "deleteMessageById",
                query = "delete " +
                        "FROM messages where id = :id"
        ),
        @NamedNativeQuery(
                name = "findMessagesByChat",
                query = "select * " +
                        "FROM messages where chat_id = :chat_id order by created_date desc",
                resultClass = MessageDTO.class
        ),
        @NamedNativeQuery(
                name = "createMessageManually",
                query = " insert into  " +
                        " messages (chat_id, created_by, created_date, message) " +
                        " values " +
                        " (:chatId, :createdBy, :createdDate, :message)"
        )
})
public class MessageDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "chat_id", nullable = false)
    private ChatDTO chat;

    @Column(name = "created_date")
    private Date createdDate;

    @ManyToOne(cascade = CascadeType.ALL, targetEntity = UserDTO.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by")
    private UserDTO createdBy;

    private String message;

    public Integer getId() {
        return id;
    }

    public MessageDTO setId(Integer id) {
        this.id = id;
        return this;
    }

    public ChatDTO getChat() {
        return chat;
    }

    public MessageDTO setChat(ChatDTO chat) {
        this.chat = chat;
        return this;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public MessageDTO setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public UserDTO getCreatedBy() {
        return createdBy;
    }

    public MessageDTO setCreatedBy(UserDTO createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public MessageDTO setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getDateFormatted() {
        return new SimpleDateFormat("dd/MM/yyyy hh:mm").format(this.createdDate);
    }

}
