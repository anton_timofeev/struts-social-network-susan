package com.networking.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

@Entity
@Table(name = "friends")

@NamedNativeQueries({
        @NamedNativeQuery(
                name = "loadAllUserFriendship",
                query = "select * from friends where user_id = :userId ",
                resultClass = FriendDTO.class
        ),
        @NamedNativeQuery(
                name = "saveFriendship",
                query = "insert into friends (user_id, friend_id) values (:userId, :friendId)",
                resultClass = UserDTO.class
        ),
        @NamedNativeQuery(
                name = "deleteFriendShip",
                query = "delete from friends where user_id = :userId and friend_id = :friendId"
        ),
        @NamedNativeQuery(
                name = "getFriends",
                query = "select * from users where id in (select friend_id from friends where user_id = :userId)",
                resultClass = UserDTO.class
        ),
        @NamedNativeQuery(
                name = "findFriendshipById",
                query = "select * from friends where id = :id",
                resultClass = FriendDTO.class
        )
})
public class FriendDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserDTO user;

    @ManyToOne
    @JoinColumn(name = "friend_id", nullable = false)
    private UserDTO friend;

    public Integer getId() {
        return id;
    }

    public FriendDTO setId(Integer id) {
        this.id = id;
        return this;
    }

    public UserDTO getUser() {
        return user;
    }

    public FriendDTO setUser(UserDTO user) {
        this.user = user;
        return this;
    }

    public UserDTO getFriend() {
        return friend;
    }

    public FriendDTO setFriend(UserDTO friend) {
        this.friend = friend;
        return this;
    }
}
