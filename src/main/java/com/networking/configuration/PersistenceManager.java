package com.networking.configuration;

import javax.persistence.CacheStoreMode;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;

public enum PersistenceManager {
    INSTANCE;

    private EntityManagerFactory emFactory;

    PersistenceManager() {
        Map properties = new HashMap(1);
        properties.put("javax.persistence.cache.storeMode", CacheStoreMode.BYPASS);
        emFactory = Persistence.createEntityManagerFactory("ssn-jpa",
                properties);
    }

    public EntityManager getEntityManager() {

        return emFactory.createEntityManager();
    }

    public void close() {
        emFactory.close();
    }

    public static PersistenceManager getInstance() {
        PersistenceManager.INSTANCE.getEntityManager().getEntityManagerFactory()
                .getCache().evictAll();
        return PersistenceManager.INSTANCE;
    }
}
