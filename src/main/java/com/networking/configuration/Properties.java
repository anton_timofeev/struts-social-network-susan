package com.networking.configuration;

import java.io.IOException;

public class Properties {
    public static final String PROPERTY_DB_DRIVER   = "db.driver";
    public static final String PROPERTY_DB_URL      = "db.url";
    public static final String PROPERTY_DB_USERNAME = "db.username";
    public static final String PROPERTY_DB_PASSWORD = "db.password";
    public static final String PROPERTY_SESSION_TIMEOUT = "session.timeout";

    private static final java.util.Properties PROPERTIES;

    static {
        PROPERTIES = new java.util.Properties();
        try {
            PROPERTIES.load(Properties.class.getResourceAsStream("/application.properties"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String get(String name) {
        Object val = PROPERTIES.get(name);
        return val != null ? String.valueOf(val) : null;
    }

    public static int getInt(String name) {
        Object val = PROPERTIES.get(name);
        return val != null ? Integer.valueOf(String.valueOf(val)) : null;
    }
}
