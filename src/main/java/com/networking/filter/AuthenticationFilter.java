package com.networking.filter;

import com.networking.exception.UnauthorizedAccessException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthenticationFilter implements Filter {

    // TODO: add ignore list
    // TODO: where it should be stored? DB
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String requestURL = request.getRequestURI();
        requestURL = requestURL.replace(request.getContextPath(), "");
        if (!"/".equalsIgnoreCase(requestURL)
                && !"/index.jsp".equalsIgnoreCase(requestURL)
                && !"/login.action".equalsIgnoreCase(requestURL)
                && !"/login".equalsIgnoreCase(requestURL)
                && !"/register.action".equalsIgnoreCase(requestURL)
                && !"/favicon.ico".equalsIgnoreCase(requestURL)
                && !"/error.jsp".equalsIgnoreCase(requestURL)
                && !"/register.jsp".equalsIgnoreCase(requestURL)
                && !requestURL.startsWith("/websocket")
                && !requestURL.startsWith("/css/")
                && !requestURL.startsWith("/js/")) {
            HttpSession session = request.getSession();
            if (session.getAttribute("user") == null) {
                throw new UnauthorizedAccessException("Unathorized access.");
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
