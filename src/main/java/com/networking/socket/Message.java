package com.networking.socket;

import com.networking.dto.UserDTO;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Message {

    private Integer id;

    private String message;

    private String userName;

    private Integer userId;

    private Date createdDate;

    private String dateFormatted;

    private Integer chatId;

    public String getMessage() {
        return message;
    }

    public Message setMessage(String message) {
        this.message = message;
        return this;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Message setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
        this.dateFormatted = new SimpleDateFormat("dd/MM/yyyy hh:mm").format(this.createdDate);
        return this;
    }

    public String getUserName() {
        return userName;
    }

    public Message setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public Integer getUserId() {
        return userId;
    }

    public Message setUserId(Integer userId) {
        this.userId = userId;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public Message setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getDateFormatted() {
        return this.dateFormatted;
    }

    public Integer getChatId() {
        return chatId;
    }

    public Message setChatId(Integer chatId) {
        this.chatId = chatId;
        return this;
    }
}
