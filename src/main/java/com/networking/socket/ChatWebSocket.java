package com.networking.socket;

import com.networking.dto.ChatDTO;
import com.networking.dto.MessageDTO;
import com.networking.service.ChatService;
import com.networking.service.MessageService;
import com.networking.service.UserService;
import org.apache.commons.lang3.math.NumberUtils;

import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

@ServerEndpoint(value = "/websocket/chat/{chatId}",
        decoders = MessageDecoder.class,
        encoders = MessageEncoder.class)
public class ChatWebSocket {

    private Session session;
    private static Set<ChatWebSocket> chatEndpoints = new CopyOnWriteArraySet<>();
    private static HashMap<String, ChatDTO> chats = new HashMap<>();
    private ChatService chatService = ChatService.getInstance();
    private UserService userService = UserService.getInstance();
    private MessageService messageService = MessageService.getInstance();

    @OnOpen
    public void onOpen(
            Session session,
            @PathParam("chatId") Integer chatId) {

        this.session = session;
        chatEndpoints.add(this);
        ChatDTO chat = chatService.find(chatId);
        chats.put(session.getId(), chat);
    }


    @OnMessage
    public void onMessage(Session session, Message message) {
        ChatDTO chat = chats.get(session.getId());
        message.setCreatedDate(new Date());
        MessageDTO newMessage = messageService.create(chat.getId(), message.getUserId(), message.getMessage());
        message.setId(newMessage.getId());
        broadcast(message);
    }

    @OnClose
    public void onClose(Session session) throws IOException, EncodeException {

        chatEndpoints.remove(this);
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        // Do error handling here
    }


    private static void broadcast(Message message) {

        chatEndpoints.forEach(endpoint -> {
            synchronized (endpoint) {
                try {
                    if (message.getChatId() != null
                            && message.getChatId().equals(chats.get(endpoint.session.getId()).getId())) {
                        endpoint.session.getBasicRemote().
                                sendObject(message);
                    }
                } catch (IOException | EncodeException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
