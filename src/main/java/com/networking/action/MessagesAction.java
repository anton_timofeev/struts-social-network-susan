package com.networking.action;

import com.networking.dto.ChatDTO;
import com.networking.dto.FriendDTO;
import com.networking.dto.MessageDTO;
import com.networking.dto.UserDTO;
import com.networking.service.ChatService;
import com.networking.service.FriendService;
import com.networking.service.MessageService;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.interceptor.SessionAware;

import java.util.List;
import java.util.Map;

public class MessagesAction extends ActionSupport implements SessionAware {

    private MessageService messageService = MessageService.getInstance();
    private FriendService friendService = FriendService.getInstance();
    private ChatService chatService = ChatService.getInstance();

    private Map<String, Object> sessionMap;

    private List<MessageDTO> messages;

    private List<FriendDTO> chats;

    private ChatDTO chat;

    private Integer friendId;

    private Integer messageId;

    private Integer chatId;

    private String message;

    private MessageDTO messageObj;

    @Override
    public void setSession(Map<String, Object> map) {
        sessionMap = map;
    }

    public String loadWholeChat() {
        UserDTO user = (UserDTO) sessionMap.get("user");
        chat = chatService.find(user.getId(), friendId);
        messages = messageService.loadWholeChat(chat.getId());
        return "SUCCESS";
    }

    public String loadFriends() {
        UserDTO user = (UserDTO) sessionMap.get("user");
        chats = friendService.getFriendships(user.getId());
        return "SUCCESS";
    }

    public String create() {
        UserDTO user = (UserDTO) sessionMap.get("user");
        messageObj = messageService.createMessage(chatId, user, message);
        return "SUCCESS";
    }

    public String delete() {
        messageService.deleteMessage(messageId);
        return "SUCCESS";
    }

    public Integer getMessageId() {
        return messageId;
    }

    public MessagesAction setMessageId(Integer messageId) {
        this.messageId = messageId;
        return this;
    }

    public Integer getChatId() {
        return chatId;
    }

    public MessagesAction setChatId(Integer chatId) {
        this.chatId = chatId;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public MessagesAction setMessage(String message) {
        this.message = message;
        return this;
    }

    public MessageDTO getMessageObj() {
        return messageObj;
    }

    public MessagesAction setMessageObj(MessageDTO messageObj) {
        this.messageObj = messageObj;
        return this;
    }

    public List<MessageDTO> getMessages() {
        return messages;
    }

    public MessagesAction setMessages(List<MessageDTO> messages) {
        this.messages = messages;
        return this;
    }

    public List<FriendDTO> getChats() {
        return chats;
    }

    public MessagesAction setChats(List<FriendDTO> chats) {
        this.chats = chats;
        return this;
    }

    public Integer getFriendId() {
        return friendId;
    }

    public MessagesAction setFriendId(Integer friendId) {
        this.friendId = friendId;
        return this;
    }

    public ChatDTO getChat() {
        return chat;
    }

    public MessagesAction setChat(ChatDTO chat) {
        this.chat = chat;
        return this;
    }
}