package com.networking.action;

import com.networking.dto.UserDTO;
import com.networking.service.FriendService;
import com.networking.service.UserService;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;

import java.util.List;
import java.util.Map;

public class FriendsAction extends ActionSupport implements SessionAware {

    public static final String SESSION_ATTRIBUTE_USER = "user";

    private UserService userService = UserService.getInstance();
    private FriendService friendService = FriendService.getInstance();

    private UserDTO user;
    private Integer friendId;

    private UserDTO friend;
    private String friendName;

    private List<UserDTO> friends;

    private List<UserDTO> availableusers;

    private String newFriendUserName;

    private SessionMap<String, Object> sessionmap;

    @Override
    public void setSession(Map<String, Object> map) {
        sessionmap = (SessionMap) map;
    }

    public String show() {
        user = (UserDTO) sessionmap.get(SESSION_ATTRIBUTE_USER);
        friends = friendService.getFriends(user.getId());
        return "SUCCESS";
    }

    public String find() {
        user = (UserDTO) sessionmap.get(SESSION_ATTRIBUTE_USER);
        availableusers = userService.getAvailableUsers(user.getId());
        return "SUCCESS";
    }

    public void follow() {
        user = (UserDTO) sessionmap.get(SESSION_ATTRIBUTE_USER);
        friendService.follow(user.getId(), friendId);
    }

    public void unfollow() {
        user = (UserDTO) sessionmap.get(SESSION_ATTRIBUTE_USER);
        friendService.unfollow(user.getId(), friendId);
    }

    public String gotoFriend() {
        friend = userService.getUser(friendId);
        return friend != null ? "SUCCESS" : "FAILURE";
    }

    public UserDTO getUser() {
        return user;
    }

    public FriendsAction setUser(UserDTO user) {
        this.user = user;
        return this;
    }

    public List<UserDTO> getAvailableusers() {
        return availableusers;
    }

    public FriendsAction setAvailableusers(List<UserDTO> availableusers) {
        this.availableusers = availableusers;
        return this;
    }

    public UserDTO getFriend() {
        return friend;
    }

    public FriendsAction setFriend(UserDTO friend) {
        this.friend = friend;
        return this;
    }

    public String getFriendName() {
        return friendName;
    }

    public FriendsAction setFriendName(String friendName) {
        this.friendName = friendName;
        return this;
    }

    public String getNewFriendUserName() {
        return newFriendUserName;
    }

    public FriendsAction setNewFriendUserName(String newFriendUserName) {
        this.newFriendUserName = newFriendUserName;
        return this;
    }

    public List<UserDTO> getFriends() {
        return friends;
    }

    public FriendsAction setFriends(List<UserDTO> friends) {
        this.friends = friends;
        return this;
    }

    public Integer getFriendId() {
        return friendId;
    }

    public FriendsAction setFriendId(Integer friendId) {
        this.friendId = friendId;
        return this;
    }
}