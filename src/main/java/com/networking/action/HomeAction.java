package com.networking.action;

import com.networking.dto.UserDTO;
import com.networking.service.UserService;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;

import java.util.Map;

public class HomeAction extends ActionSupport implements SessionAware {

    public static final String SESSION_ATTRIBUTE_USER = "user";

    private UserService userService = UserService.getInstance();

    private UserDTO user;

    private SessionMap<String, Object> sessionmap;

    @Override
    public void setSession(Map<String, Object> map) {
        sessionmap = (SessionMap) map;
    }

    public String execute() {
        user = (UserDTO) sessionmap.get(SESSION_ATTRIBUTE_USER);
        user = userService.getUser(user.getId());
        return "SUCCESS";
    }

    public UserDTO getUser() {
        return user;
    }

    public HomeAction setUser(UserDTO user) {
        this.user = user;
        return this;
    }
}