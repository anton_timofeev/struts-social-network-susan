package com.networking.action;

import com.networking.dto.PostDTO;
import com.networking.dto.UserDTO;
import com.networking.service.PostService;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;

import java.util.Map;

public class PostsAction extends ActionSupport implements SessionAware {

    private Integer wallId;
    private Integer userId;
    private String content;

    private Integer postId;

    private PostDTO post;

    private PostService postService = PostService.getInstance();

    private SessionMap<String, Object> sessionmap;

    @Override
    public void setSession(Map<String, Object> map) {
        sessionmap = (SessionMap) map;
    }

    public String create() {
        UserDTO user = (UserDTO) sessionmap.get("user");
        post = postService.createNewPost(wallId, user.getId(), content);
        return "SUCCESS";
    }

    public void delete() {
        postService.deletePost(postId);
    }

    public PostDTO getPost() {
        return post;
    }

    public PostsAction setPost(PostDTO post) {
        this.post = post;
        return this;
    }

    public Integer getWallId() {
        return wallId;
    }

    public PostsAction setWallId(Integer wallId) {
        this.wallId = wallId;
        return this;
    }

    public Integer getUserId() {
        return userId;
    }

    public PostsAction setUserId(Integer userId) {
        this.userId = userId;
        return this;
    }

    public String getContent() {
        return content;
    }

    public PostsAction setContent(String content) {
        this.content = content;
        return this;
    }

    public Integer getPostId() {
        return postId;
    }

    public PostsAction setPostId(Integer postId) {
        this.postId = postId;
        return this;
    }
}