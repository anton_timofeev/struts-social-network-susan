package com.networking.action;

import com.networking.service.UserService;

public class UserAction {
    private UserService userService = UserService.getInstance();
    private String username;

    private String firstName;

    private String lastName;

    private String email;

    private String password;

    public String register() {
        try {
            userService.createUser(username, email, firstName, lastName, password);
            return "SUCCESS";
        } catch (Exception ex) {
            return "FAILURE";
        }
    }

    public String getUsername() {
        return username;
    }

    public UserAction setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public UserAction setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public UserAction setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UserAction setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserAction setPassword(String password) {
        this.password = password;
        return this;
    }
}
