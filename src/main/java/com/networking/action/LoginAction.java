package com.networking.action;

import com.networking.dto.UserDTO;
import com.networking.service.UserService;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;

import java.util.Map;

public class LoginAction extends ActionSupport implements SessionAware {

    public static final String SESSION_ATTRIBUTE_USER = "user";
    private String login;

    private String password;

    private UserDTO user;

    private UserService userService = UserService.getInstance();

    private SessionMap<String, Object> sessionmap;

    @Override
    public void setSession(Map<String, Object> map) {
        sessionmap = (SessionMap) map;
    }

    public String execute() {

        // get request parameters for username and password
        UserDTO user;
        if ((user = userService.getUser(login, password)) != null) {
            //generate a new session

            sessionmap.put(SESSION_ATTRIBUTE_USER, user);
            this.user = user;
            return "SUCCESS";
        }
        return "FAILURE";
    }

    public String getLogin() {
        return login;
    }

    public LoginAction setLogin(String login) {
        this.login = login;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public LoginAction setPassword(String password) {
        this.password = password;
        return this;
    }

    public UserDTO getUser() {
        return user;
    }

    public LoginAction setUser(UserDTO user) {
        this.user = user;
        return this;
    }
}