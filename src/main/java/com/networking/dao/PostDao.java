package com.networking.dao;

import com.networking.configuration.PersistenceManager;
import com.networking.dto.PostDTO;
import com.networking.dto.WallDTO;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class PostDao extends GenericDao<PostDTO>{
    private static final PostDao INSTANCE;

    static {
        INSTANCE = new PostDao();
    }
    private PersistenceManager pm = PersistenceManager.INSTANCE;

    private PostDao() {
    }

    public PostDTO create(PostDTO post) {
        EntityManager em = pm.getEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(post);
        em.flush();
        tx.commit();

        em.close();
        return post;
    }

    public void delete(Integer id) {
        this.executeInTRX(em -> {
            em.createNamedQuery("deletePostById").setParameter("id", id).executeUpdate();
        });
    }

    public static PostDao getInstance() {
        return INSTANCE;
    }
}
