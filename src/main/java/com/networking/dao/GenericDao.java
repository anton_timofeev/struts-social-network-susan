package com.networking.dao;

import com.networking.configuration.PersistenceManager;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.function.Consumer;
import java.util.function.Function;

public class GenericDao<T> {

    private PersistenceManager pm = PersistenceManager.getInstance();

    public T update(T entity) {
        EntityManager em = pm.getEntityManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            em.merge(entity);
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            throw ex;
        } finally {
            em.close();
        }
        return entity;
    }

    public void executeInTRX(Consumer<EntityManager> consumer) {

        EntityManager em = pm.getEntityManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            consumer.accept(em);
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            throw ex;
        } finally {
            em.close();
        }
    }

    public <Y> Y executeInTRXWithResult(Function<EntityManager, Y> consumer) {

        EntityManager em = pm.getEntityManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            Y result = consumer.apply(em);
            tx.commit();
            return result;
        } catch (Exception ex) {
            tx.rollback();
            throw ex;
        } finally {
            em.close();
        }
    }
}
