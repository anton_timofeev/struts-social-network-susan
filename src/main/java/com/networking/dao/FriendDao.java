package com.networking.dao;

import com.networking.dto.FriendDTO;
import com.networking.dto.UserDTO;

import java.util.List;

public class FriendDao extends GenericDao<FriendDTO>{

    private static final FriendDao INSTANCE;

    static {
        INSTANCE = new FriendDao();
    }

    private FriendDao() {
    }

    public FriendDTO getFriendship(Integer id) {
        return (FriendDTO) this.executeInTRXWithResult(em ->
                em.createNamedQuery("findFriendshipById")
                        .setParameter("id", id).getSingleResult());
    }


    public List<FriendDTO> loadAllUserFriendship(Integer userId) {
        return this.executeInTRXWithResult(em ->
                em.createNamedQuery("loadAllUserFriendship")
                        .setParameter("userId", userId).getResultList());
    }

    public void saveFriendship(Integer userId, Integer friendId) {
        this.executeInTRX(em -> {
            em.createNamedQuery("saveFriendship")
                    .setParameter("friendId", friendId)
                    .setParameter("userId", userId).executeUpdate();
        });
    }

    public List<UserDTO> getFriends(Integer userId) {
        return this.executeInTRXWithResult((em) ->
                em.createNamedQuery("getFriends", UserDTO.class)
                        .setParameter("userId", userId).getResultList());
    }

    public void deleteFriendShip(Integer userId, Integer friendId) {
        this.executeInTRX(em -> em.createNamedQuery("deleteFriendShip")
                .setParameter("userId", userId)
                .setParameter("friendId", friendId)
                .executeUpdate());
    }

    public static FriendDao getInstance() {
        return INSTANCE;
    }
}
