package com.networking.dao;

import com.networking.configuration.PersistenceManager;
import com.networking.dto.UserDTO;
import com.networking.dto.WallDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class WallDao extends GenericDao<WallDTO> {
    private static final WallDao INSTANCE;

    static {
        INSTANCE = new WallDao();
    }

    private WallDao() {
    }


    public WallDTO create(WallDTO wall) {
        this.executeInTRX(em -> {
            em.persist(wall);
        });
        return wall;
    }

    public WallDTO get(Integer userId) {
        PersistenceManager pm = PersistenceManager.INSTANCE;
        EntityManager em = pm.getEntityManager();
        WallDTO wall = em.createNamedQuery("getWallById", WallDTO.class)
                .setParameter("wallId", userId)
                .getResultList().stream().findFirst().orElse(null);
        em.close();
        return wall;
    }

    public static WallDao getInstance() {
        return INSTANCE;
    }
}
