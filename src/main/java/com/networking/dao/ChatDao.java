package com.networking.dao;

import com.networking.dto.ChatDTO;

public class ChatDao extends GenericDao<ChatDTO> {

    private static final ChatDao INSTANCE;

    static {
        INSTANCE = new ChatDao();
    }

    private ChatDao() {
    }

    public ChatDTO findChatByUsers(Integer userId1, Integer userId2) {
        return this.executeInTRXWithResult(em ->
                em.createNamedQuery("findChatByUsers", ChatDTO.class)
                        .setParameter("userId1", userId1)
                        .setParameter("userId2", userId2)
                        .getResultList().stream().findFirst().orElse(null));
    }

    public ChatDTO findChatById(Integer id) {
        return this.executeInTRXWithResult(em ->
                em.createNamedQuery("findChatById", ChatDTO.class)
                        .setParameter("id", id)
                        .getSingleResult());
    }

    public void createChat(Integer userId1, Integer userId2) {
        this.executeInTRX(em ->
                em.createNamedQuery("createChat")
                        .setParameter("userId1", userId1)
                        .setParameter("userId2", userId2)
                        .executeUpdate());
    }


    public static ChatDao getInstance() {
        return INSTANCE;
    }
}
