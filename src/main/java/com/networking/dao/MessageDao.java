package com.networking.dao;

import com.networking.configuration.PersistenceManager;
import com.networking.dto.MessageDTO;
import com.networking.dto.PostDTO;
import org.hibernate.Session;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.Date;
import java.util.List;

public class MessageDao extends GenericDao<MessageDTO> {
    private static final MessageDao INSTANCE;

    static {
        INSTANCE = new MessageDao();
    }

    private PersistenceManager pm = PersistenceManager.INSTANCE;

    private MessageDao() {
    }

    public List<MessageDTO> load(Integer chatId) {
        return this.executeInTRXWithResult(em ->
                em.createNamedQuery("findMessagesByChat", MessageDTO.class)
                        .setParameter("chat_id", chatId).getResultList());
    }

    public MessageDTO create(MessageDTO post) {
        this.executeInTRX(em -> em.merge(post));
        return post;
    }

    public void create(Integer chatId, Integer createdBy, String message) {
        this.executeInTRX(em -> {
            em.createNamedQuery("createMessageManually")
                    .setParameter("chatId", chatId)
                    .setParameter("createdBy", createdBy)
                    .setParameter("message", message)
                    .setParameter("createdDate", new Date()).executeUpdate();
        });
    }

    public void delete(Integer id) {
        this.executeInTRX(em -> {
            em.createNamedQuery("deleteMessageById").setParameter("id", id).executeUpdate();
        });
    }

    public static MessageDao getInstance() {
        return INSTANCE;
    }
}
