package com.networking.dao;

import com.networking.configuration.PersistenceManager;
import com.networking.dto.UserDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class UserDao extends GenericDao<UserDTO> {

    private static final UserDao INSTANCE;

    static {
        INSTANCE = new UserDao();
    }

    private PersistenceManager pm = PersistenceManager.getInstance();

    private UserDao() {
    }

    public UserDTO create(UserDTO user) {
        this.executeInTRX(em -> {
            em.persist(user);
        });
        return user;
    }

    public UserDTO get(String username) {
        PersistenceManager pm = PersistenceManager.getInstance();
        EntityManager em = pm.getEntityManager();
        em.getEntityManagerFactory().getCache().evictAll();
        UserDTO user = em.createNamedQuery("getUserByUsername", UserDTO.class)
                .setParameter("username", username)
                .getResultList().stream().findFirst().orElse(null);
        em.close();
        return user;
    }

    public UserDTO get(Integer userId) {
        EntityManager em = pm.getEntityManager();
        em.getTransaction().begin();
        UserDTO user = em.createNamedQuery("getUserById", UserDTO.class)
                .setParameter("userId", userId)
                .getResultList().stream().findFirst().orElse(null);
        em.getTransaction().commit();
        em.close();
        return user;
    }

    public UserDTO get(String username, byte[] hashPassword) {
        EntityManager em = pm.getEntityManager();
        UserDTO user = em.createNamedQuery("getUserByUsernamePassword", UserDTO.class)
                .setParameter("username", username)
                .setParameter("password", hashPassword)
                .getResultList().stream().findFirst().orElse(null);
        em.close();
        return user;
    }

    public List<UserDTO> getAvailableForFriendship(Integer userId) {
        EntityManager em = pm.getEntityManager();
        em.getTransaction().begin();
        List<UserDTO> userList = em.createNamedQuery(
                "getNotFriends", UserDTO.class).
                setParameter("userId", userId).getResultList();
        em.getTransaction().commit();
        em.close();
        return userList;
    }

    public static UserDao getInstance() {
        return INSTANCE;
    }
}
