package com.networking.service;

import com.networking.dao.FriendDao;
import com.networking.dto.FriendDTO;
import com.networking.dto.UserDTO;

import java.util.List;

public class FriendService {
    private static final FriendService INSTANCE;

    static {
        INSTANCE = new FriendService();
    }

    private FriendDao friendDao = FriendDao.getInstance();

    private FriendService() {
    }

    public void follow(Integer userId, Integer friendId) {
        friendDao.saveFriendship(userId, friendId);
    }

    public void unfollow(Integer userId, Integer friendId) {
        friendDao.deleteFriendShip(userId, friendId);
    }

    public List<UserDTO> getFriends(Integer userId) {
        return friendDao.getFriends(userId);
    }

    public List<FriendDTO> getFriendships(Integer userId) {
        return friendDao.loadAllUserFriendship(userId);
    }

    public FriendDTO getFriendship(Integer id) {
        return friendDao.getFriendship(id);
    }

    public static FriendService getInstance() {
        return INSTANCE;
    }
}
