package com.networking.service;

import com.networking.dao.UserDao;
import com.networking.dao.WallDao;
import com.networking.dto.FriendDTO;
import com.networking.dto.UserDTO;
import com.networking.dto.WallDTO;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UserService {
    private static final UserService INSTANCE;
    static {
        INSTANCE = new UserService();
    }
    private UserService() {}

    private UserDao userDao = UserDao.getInstance();
    private WallDao wallDao = WallDao.getInstance();
    private PasswordEncryptionService passwordEncryptionService = PasswordEncryptionService.getInstance();

    public void createUser(String userName, String email, String firstName, String lastName, String password) {
        try {
            byte[] salt = passwordEncryptionService.generateSalt();
            byte[] hashPassword = passwordEncryptionService.getEncryptedPassword(password, salt);
            UserDTO user = new UserDTO()
                    .setUsername(userName)
                    .setEmail(email)
                    .setFirstName(firstName)
                    .setLastName(lastName)
                    .setSalt(salt)
                    .setHashPassword(hashPassword);
            Set<WallDTO> walls = new HashSet<>(1);
            walls.add(new WallDTO().setUser(user));
            user.setWalls(walls);
            userDao.create(user);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public boolean checkUser(String username, String password) {
        return getUser(username, password) != null;
    }

    public UserDTO getUser(String username, String password) {
        try {
            UserDTO foundByUserName = userDao.get(username);
            if (foundByUserName == null) {
                return null;
            }
            byte[] salt = foundByUserName.getSalt();
            byte[] hashPassword = passwordEncryptionService.getEncryptedPassword(password, salt);
            UserDTO foundByUserPassword = userDao.get(username, hashPassword);
            if (foundByUserPassword == null) {
                return null;
            }

            foundByUserPassword.setHashPassword(null);
            foundByUserPassword.setSalt(null);
            return foundByUserPassword;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public List<UserDTO> getAvailableUsers(Integer userId) {
        return userDao.getAvailableForFriendship(userId);
    }

    public UserDTO getUser(String username) {
        return userDao.get(username);
    }

    public UserDTO getUser(Integer userId) {
        return userDao.get(userId);
    }

    public static UserService getInstance() {
        return INSTANCE;
    }
}
