package com.networking.service;

import com.networking.dao.WallDao;
import com.networking.dto.WallDTO;

public class WallService {
    private static final WallService INSTANCE;

    static {
        INSTANCE = new WallService();
    }

    private WallDao wallDao = WallDao.getInstance();

    private WallService() {
    }

    public WallDTO getWall(Integer wallId) {
        return wallDao.get(wallId);
    }

    public static WallService getInstance() {
        return INSTANCE;
    }
}
