package com.networking.service;

import com.networking.dao.ChatDao;
import com.networking.dto.ChatDTO;

public class ChatService {
    private static final ChatService INSTANCE;

    static {
        INSTANCE = new ChatService();
    }

    private ChatDao chatDao = ChatDao.getInstance();

    private ChatService() {
    }

    public void create(Integer userId1, Integer userId2) {
        chatDao.createChat(userId1, userId2);
    }

    public ChatDTO find(Integer id) {
        return chatDao.findChatById(id);
    }

    public ChatDTO find(Integer userId1, Integer userId2) {
        ChatDTO chat = chatDao.findChatByUsers(userId1, userId2);
        if (chat == null) {
            create(userId1, userId2);
            chat = chatDao.findChatByUsers(userId1, userId2);
        }
        return chat;
    }

    public static ChatService getInstance() {
        return INSTANCE;
    }
}
