package com.networking.service;

import com.networking.dao.PostDao;
import com.networking.dto.PostDTO;
import com.networking.dto.UserDTO;
import com.networking.dto.WallDTO;

import java.util.Date;

public class PostService {
    private static final PostService INSTANCE;

    static {
        INSTANCE = new PostService();
    }
    private PostDao postDao = PostDao.getInstance();
    private UserService userService = UserService.getInstance();
    private WallService wallService = WallService.getInstance();

    private PostService() {
    }

    public PostDTO createNewPost(Integer wallId, Integer userId, String content) {
        WallDTO wall = wallService.getWall(wallId);
        UserDTO user = userService.getUser(userId);
        PostDTO post = new PostDTO()
                .setContent(content)
                .setWall(wall)
                .setCreatedDate(new Date())
                .setCreatedBy(user);
        postDao.update(post);
        return post;
    }

    public void deletePost(Integer id) {
        postDao.delete(id);
    }

    public static PostService getInstance() {
        return INSTANCE;
    }
}
