package com.networking.service;

import com.networking.dao.MessageDao;
import com.networking.dao.PostDao;
import com.networking.dto.FriendDTO;
import com.networking.dto.MessageDTO;
import com.networking.dto.PostDTO;
import com.networking.dto.UserDTO;
import com.networking.dto.WallDTO;

import java.util.Date;
import java.util.List;

public class MessageService {
    private static final MessageService INSTANCE;

    static {
        INSTANCE = new MessageService();
    }
    private MessageDao messageDao = MessageDao.getInstance();
    private UserService userService = UserService.getInstance();
    private FriendService friendService = FriendService.getInstance();
    private ChatService chatService = ChatService.getInstance();

    private MessageService() {
    }

    public void createNewMessage(Integer chatId, Integer createdBy, String message) {
        messageDao.create(chatId, createdBy, message);
    }

    public MessageDTO create(Integer chatId, Integer createdBy, String message) {
        MessageDTO messagedto = new MessageDTO()
                .setMessage(message)
                .setChat(chatService.find(chatId))
                .setCreatedBy(userService.getUser(createdBy))
                .setCreatedDate(new Date());
        return messageDao.create(messagedto);
    }

    public MessageDTO createMessage(Integer chatId, UserDTO user, String message) {
        return messageDao.create(new MessageDTO()
                .setCreatedBy(user)
                .setChat(chatService.find(chatId))
                .setMessage(message));
    }

    public void deleteMessage(Integer id) {
        messageDao.delete(id);
    }

    public List<MessageDTO> loadWholeChat(Integer chatId) {
        return messageDao.load(chatId);
    }

    public static MessageService getInstance() {
        return INSTANCE;
    }
}
